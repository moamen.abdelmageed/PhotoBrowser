//
//  TrendingImagesViewModel.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import SwiftMessages

protocol TrendingImagesViewControllerDelegate: NSObjectProtocol {
    func trendingImagesRequestWillSend()
    func trendingImagesRequestSucceeded(photos: [Photo], currentPage: Int, totalPage: Int)
    func trendingImagesRequestFailed(photos: [Photo])
}

class TrendingImagesViewModel: NSObject, TrendingImagesBackendDelegate {
    
    weak var delegate:TrendingImagesViewControllerDelegate?
    lazy var databaseManager = DatabaseManager()
    let backendManager = BackendManager()

    
    public func getFlickrTrendingImagesList(nextPage:Int) {
        delegate?.trendingImagesRequestWillSend()
        backendManager.getTrendingImagesListData(delegate: self, pageNumber: nextPage)
    }
    
    
    func trendingImagesRequestSucceeded(data: [JSON], currentPage: Int, totalPage: Int) {
        
        var photoArray = [Photo]()

        if currentPage == 1  {
            databaseManager.deleteFlickrTrendingImagesData()
        }
        
        for photo in data {
            let photoModel = Photo()
            photoModel.mapping(map: Map(mappingType: .fromJSON, JSON: photo.dictionaryObject!))
            databaseManager.saveFlickrTrendingImagesData(data: photoModel)
            photoArray.append(photoModel)
        }
        
        delegate?.trendingImagesRequestSucceeded(photos: photoArray, currentPage: currentPage, totalPage: totalPage)
    }
    
    
    func trendingImagesRequestUnExpectedResponse() {
        
        SwiftMessages.showMessage(title: "Erroe", body: "Unexpected Response", type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 2)
    }
    
    
    func trendingImagesRequestFailed() {
        delegate?.trendingImagesRequestFailed(photos: databaseManager.getFlickrTrendingPhotosData())
    }

}
