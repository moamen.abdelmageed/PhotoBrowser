//
//  Utilities.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import Kingfisher
import Localize_Swift

class Utilities: NSObject {
    
    
    private static var container: UIView = UIView()
    private static var loadingView: UIView = UIView()
    private static var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    class func showActivityIndicator(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor =  UIColor.black.withAlphaComponent(0.3)
        container.restorationIdentifier = "activityIndicator"
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height :40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    class func hideActivityIndicator(uiView: UIView) {
        for subview in uiView.subviews {
            if subview.restorationIdentifier == "activityIndicator" {
                subview.fadeOut(0.3, delay: 0.0, completion: { _ in subview
                    .removeFromSuperview()
                })
            }
        }
    }
    
    
    
    class func downloadImage(path: URL?, placeholder: UIImage, into imageView: UIImageView, indicator: UIActivityIndicatorView?) {
        
        ImageCache.default.maxDiskCacheSize = Utilities.getImageCashingSize()
        if let imagePath = path {
            imageView.kf.setImage(with: imagePath, placeholder: placeholder, options: nil, progressBlock: nil) {
                (image, error, disk, url) in
                indicator?.removeFromSuperview()
            }
        }
    }
    
    class func getImageCashingSize() -> UInt {
        return UInt(50 * 1024 * 1024)
    }
    

    
    class func getMD5(string : String) -> String {
        let data = MD5(string: string)
        let string = getMD5String(data: data)
        return string
    }
    
    class func getMD5String(data: Data) -> String {
        let md5Hex =  data.map { String(format: "%02hhx", $0) }.joined()
        return md5Hex
    }
    
    class func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    
    class func localizedString(forKey key: String) -> String {
        Localize.setCurrentLanguage("en")
        let resutl = key.localized(using: "Localizable", in: .main)
        return resutl;
    }

}
