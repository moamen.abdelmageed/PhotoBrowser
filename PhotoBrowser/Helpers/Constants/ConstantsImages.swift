//
//  ConstantsImages.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

struct ConstantsImages {
    
    static let IC_BACKGROUND = "ic_app_bg.png"
    static let IC_PHOTO_UNAVAILABLE = "ic_placeholder.png"
    static let IC_NO_IMAGES_FOUND = "ic_no_images.png"

}
