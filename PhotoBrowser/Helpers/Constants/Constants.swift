//
//  Constants.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/29/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

struct Constants {

    static let K_BASE_URL = "https://api.flickr.com/services/rest/"
    static let k_METHODE  = "flickr.interestingness.getList"
    static let K_SECRET   = "e658621bb7961ac6"
    static let K_APP_KEY  = "75da4b9fafb6c1f565e09cca9f1f25aa"
    static let K_PER_PAGE = 20
    
}
