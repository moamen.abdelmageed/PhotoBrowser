//
//  Photo.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/2/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class Photo: Object, Mappable {
    
    dynamic var photo_id:          String? = ""
    dynamic var secret:            String? = ""
    dynamic var server:            String? = ""
    dynamic var farm:              Int = 0
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        photo_id                <- map["id"]
        secret                  <- map["secret"]
        server                  <- map["server"]
        farm                    <- map["farm"]
    }
    
}
