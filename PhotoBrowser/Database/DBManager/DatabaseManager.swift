//
//  DatabaseManager.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

class DatabaseManager {
    
    var trendPhotosDatabaseRequest = TrendingPhotosDatabaseRequest()
    
   
    func saveFlickrTrendingImagesData(data: Photo){
        trendPhotosDatabaseRequest.saveTrendingImagesData(data: data)
    }
    
    
    func getFlickrTrendingPhotosData() -> [Photo] {
       return trendPhotosDatabaseRequest.getTrendingImagesData()
    }
    
    
    func deleteFlickrTrendingImagesData(){
        trendPhotosDatabaseRequest.deleteTrendingImagesData()
    }

}
