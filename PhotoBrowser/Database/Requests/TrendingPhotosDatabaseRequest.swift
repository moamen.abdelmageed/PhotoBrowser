//
//  TrendingPhotosDatabaseRequest.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import RealmSwift

class TrendingPhotosDatabaseRequest {

    var mainDispatchQueue: DispatchQueue?
    var photosArray = [Photo]()
    
    func saveTrendingImagesData(data: Photo){
        
        if mainDispatchQueue == nil{
            mainDispatchQueue = DispatchQueue.main
        }
        
        mainDispatchQueue?.async {
            let realm = try! Realm()
            realm.beginWrite()
            realm.add(data)
            try! realm.commitWrite()
        }
    }
    
    
    
    func getTrendingImagesData() -> [Photo] {
        let realm = try! Realm()
        let photoModel = realm.objects(Photo.self)
        for photo in photoModel {
            photosArray.append(photo)
        }
        return photosArray
    }
    
    

    
    func deleteTrendingImagesData() {
        
        if mainDispatchQueue == nil{
            mainDispatchQueue = DispatchQueue.main
        }
        
        mainDispatchQueue?.async {
            let realm = try! Realm()
            realm.beginWrite()
            realm.delete(realm.objects(Photo.self))
            try! realm.commitWrite()
        }
    }
    
}
