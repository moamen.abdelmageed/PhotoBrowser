//
//  BackendManager.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/2/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

class BackendManager {

    func getTrendingImagesListData(delegate: TrendingImagesBackendDelegate, pageNumber: Int) {
        let request = TrendingPhotosRequest();
        request.trendingImagesBEDelegate = delegate;
        request.getImages(pageNumber: pageNumber)
    }
    
}
