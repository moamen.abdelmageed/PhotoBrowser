//
//  TrendingPhotosRequest.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/2/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import SwiftMessages

protocol TrendingImagesBackendDelegate: NSObjectProtocol {
    func trendingImagesRequestSucceeded(data: [JSON], currentPage: Int, totalPage: Int)
    func trendingImagesRequestUnExpectedResponse()
    func trendingImagesRequestFailed()
}

class TrendingPhotosRequest {

    public var trendingImagesBEDelegate: TrendingImagesBackendDelegate?
    private var network: Networking!
    private var currentPage = 0
    private var totalPages = 0
    
    init() {
        network = Networking(requestTimeout: 15)
    }
    
    func getImages(pageNumber: Int) {
        
        let app_auth = Utilities.getMD5(string: "\(Constants.K_SECRET)api_key\(Constants.K_APP_KEY)formatjsonmethod\(Constants.k_METHODE)page\(pageNumber)per_page\(Constants.K_PER_PAGE)")
        
        let requestUrl = ("\(Constants.K_BASE_URL)?nojsoncallback=1api_sig=\(app_auth)&format=json&method=\(Constants.k_METHODE)&api_key=\(Constants.K_APP_KEY)&per_page=\(Constants.K_PER_PAGE)&page=\(pageNumber)")
        
        
        network.manager.request(requestUrl).responseJSON { response in
            
            switch response.result {
                
            case.success(let value):
                
                let json = JSON(value)
                
                if let photosObject = json["photos"].dictionary {
                    
                    if  let curPage = photosObject["page"]?.int, let totPages =  photosObject["pages"]?.int{
                        self.currentPage = curPage
                        self.totalPages = totPages
                    }
                    
                    if let photos = photosObject["photo"]?.array {
                        self.trendingImagesBEDelegate?.trendingImagesRequestSucceeded(data: photos, currentPage: self.currentPage, totalPage: self.totalPages)
                    }
                }
                    
                else {
                    self.trendingImagesBEDelegate?.trendingImagesRequestUnExpectedResponse()
                }
                break
                
                
            case.failure(let error):
                
                self.trendingImagesBEDelegate?.trendingImagesRequestFailed()
                let errorMessage = error as NSError
                SwiftMessages.showMessage(title: "Erroe", body: errorMessage.localizedDescription, type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 2)
                break
                
            }
            
        }
        
    }
}
