//
//  EmptyView.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/2/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    class func instanceFromNib(parentView: UIView) {
        let view = UINib(nibName: "EmptyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.frame = CGRect(x: 10, y: 100, width: 250, height:300)
        view.center = parentView.center
        parentView.addSubview(view)
    }

}
