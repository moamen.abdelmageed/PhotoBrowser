//
//  UIScreenExtensions.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

public struct Screen {
    
    
    public static var screenWidth: CGFloat = {
        let screenSize = UIScreen.main.bounds.size
        return screenSize.width
    }()
    
    
    public static var screenHeight: CGFloat = {
        let screenSize = UIScreen.main.bounds.size
        return screenSize.height
    }()
    
}
