//
//  RIGImageGalleryExtensions.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/31/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import RIGImageGallery

extension RIGImageGalleryViewController {

    func handleImageLoadAtIndex(_ index: Int) -> ((Data?, URLResponse?, Error?) -> Void) {
        return { [weak self] (data: Data?, response: URLResponse?, error: Error?) in
            guard let image = data.flatMap(UIImage.init), error == nil else {
                if let error = error {
                    print(error)
                }
                return
            }
            self?.images[index].isLoading = false
            self?.images[index].image = image
        }
    }
}
