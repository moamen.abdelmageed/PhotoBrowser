//
//  UIViewUIExtensions.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

extension UIView {
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}
