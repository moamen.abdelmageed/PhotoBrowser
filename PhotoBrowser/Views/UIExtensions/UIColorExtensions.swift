//
//  UIColorExtensions.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/2/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init?(_ rgbValue : UInt32, alpha: CGFloat = 1.0) {
        
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8) / 256.0
        let blue = CGFloat(rgbValue & 0xFF) / 256.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}
