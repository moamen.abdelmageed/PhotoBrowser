//
//  UIDeviceExtension.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

public extension UIDevice {
    
    public static func isPhone() -> Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    
    public static func isPad() -> Bool {
        return UIDevice().userInterfaceIdiom == .pad
    }
    
}

