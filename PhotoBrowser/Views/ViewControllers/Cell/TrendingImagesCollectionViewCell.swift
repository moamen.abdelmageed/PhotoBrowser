//
//  TrendingImagesCollectionViewCell.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/30/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

class TrendingImagesCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var flickrActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var flickerImage: UIImageView!
    
    
    // MARK: - Cell Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        handleCellStyle()
    }
    
    
    // MARK: - Cell Style
    
    func handleCellStyle() {
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 2
        layer.cornerRadius = 5
    }
    
    
    // MARK: - Setting data
    
    func setPhotoData(photoUrl: URL?) {

        if let imagePath = photoUrl {
            Utilities.downloadImage(path: imagePath, placeholder: UIImage(named: ConstantsImages.IC_PHOTO_UNAVAILABLE)!, into: flickerImage, indicator: flickrActivityIndicator)
        }
    }
}


