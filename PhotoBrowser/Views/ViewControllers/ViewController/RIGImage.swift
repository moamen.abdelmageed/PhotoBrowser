//
//  RIGImage.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import RIGImageGallery
import Kingfisher

extension ViewController {
    
    func prepareRemoteGallery() -> RIGImageGalleryViewController {
        
        let rigItems: [RIGImageGalleryItem] = flickrTrendingImagesArray.map { url in
            var rigImage : UIImage?
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image,_,_,_ in
                if let img = image {
                    rigImage = img
                }
            })
            
            return RIGImageGalleryItem(image : rigImage, placeholderImage: UIImage(named: ConstantsImages.IC_PHOTO_UNAVAILABLE)!,
                                title: url.pathComponents.last ?? "",
                                isLoading: false)
        }
        
        let rigController = RIGImageGalleryViewController(images: rigItems)
        rigController.setCurrentImage(selectedImage!, animated: false)
        return rigController
    }
    
    
    func navBarWrappedViewController(_ viewController: UIViewController) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.barStyle = .blackTranslucent
        navigationController.navigationBar.tintColor = .white
        navigationController.toolbar.barStyle = .blackTranslucent
        navigationController.toolbar.tintColor = .white
        return navigationController
    }
    
    
    func dismissPhotoViewer(_ :RIGImageGalleryViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func updateCount(_ gallery: RIGImageGalleryViewController, position: Int, total: Int) {
        gallery.countLabel.text = "\(position + 1) of \(total)"
    }
    

    func traitCollectionChangeHandler(_ photoView: RIGImageGalleryViewController) {
        let isPhone = UITraitCollection(userInterfaceIdiom: .phone)
        let isCompact = UITraitCollection(verticalSizeClass: .compact)
        let allTraits = UITraitCollection(traitsFrom: [isPhone, isCompact])
        photoView.doneButton = photoView.traitCollection.containsTraits(in: allTraits) ? nil : UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
    }
    
}
