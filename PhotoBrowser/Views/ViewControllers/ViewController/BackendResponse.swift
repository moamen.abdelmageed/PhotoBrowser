//
//  BackendResponse.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

extension ViewController: TrendingImagesViewControllerDelegate {
    
    func trendingImagesRequestWillSend() {
        if currentPageNumber == 0 {
            Utilities.showActivityIndicator(uiView: self.view)
        }
    }
    
    
    func trendingImagesRequestSucceeded(photos: [Photo], currentPage: Int, totalPage: Int) {
        
        if currentPage == 1 {
            flickrTrendingImagesArray = [].flatMap(URL.init(string:))
        }
        
        currentPageNumber = currentPage
        lastPageNumber = totalPage
        handleDataRequest(photos: photos)
    }
    
    
    func trendingImagesRequestFailed(photos: [Photo]) {
        
        flickrTrendingImagesArray = [].flatMap(URL.init(string:))
        handleDataRequest(photos: photos)
    }
    
    
    func handleDataRequest(photos: [Photo]) {
        if photos.count > 0 {
            checkRefrishControlState()
            
            for item in photos {
                flickrTrendingImagesArray.append(URL(string: "https://farm\(item.farm).staticflickr.com/" + item.server! + "/" + item.photo_id! + "_" + item.secret! + ".jpg")!)
            }
            
            collectionView.reloadData()
        }
        else {
            EmptyView.instanceFromNib(parentView: self.view)
        }
        
        Utilities.hideActivityIndicator(uiView: self.view)
    }
    
}
