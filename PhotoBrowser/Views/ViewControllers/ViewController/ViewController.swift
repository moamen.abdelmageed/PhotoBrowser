//
//  ViewController.swift
//  PhotoBrowser
//
//  Created by Mo'men on 10/29/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit

class ViewController: BaseViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    // MARK: - IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!

    
    // MARK: - Variables
    
    private var trendingImagesViewModel: TrendingImagesViewModel = TrendingImagesViewModel()
    var flickrTrendingImagesArray: [URL]!
    var selectedImage: Int?
    var currentPageNumber = 0
    var lastPageNumber = 0
    private let reuseIdentifier = "cell"

    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        trendingImagesViewModel.delegate = self;
        collectionView.addSubview(refreshControl!)
        getData(pageNumber: 1)
    }
    
    
    // MARK: - Data Calling
    
    override func getData(pageNumber: Int) {
        trendingImagesViewModel.getFlickrTrendingImagesList(nextPage: pageNumber)
    }
    
    
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count =  flickrTrendingImagesArray?.count {
            return count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! TrendingImagesCollectionViewCell

        cell.setPhotoData(photoUrl: flickrTrendingImagesArray[indexPath.row])

        // Cells animation
        let finalFrame: CGRect = cell.frame
        cell.frame = CGRect(x: finalFrame.origin.x - 1000, y: -500, width: 0, height: 0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.frame = finalFrame
        })

        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var numberOfImagesInRow : CGFloat = 3
        var paddingBetweenColumns : CGFloat = 40
        
        if UIDevice.isPad() {
            numberOfImagesInRow = 4
            paddingBetweenColumns = 50
        }
        
        let stretch = (Screen.screenWidth - paddingBetweenColumns ) / numberOfImagesInRow
        return CGSize(width: stretch, height: stretch)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedImage = indexPath.row
        let photoViewController = prepareRemoteGallery()
        photoViewController.dismissHandler = dismissPhotoViewer
        photoViewController.countUpdateHandler = updateCount
        photoViewController.traitCollectionChangeHandler = traitCollectionChangeHandler
        let navigationController = navBarWrappedViewController(photoViewController)
        present(navigationController, animated: true, completion: nil)
    }
    
    
    // MARK: - Scroll Dragging
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == collectionView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
                if(isNetworkConnected()) {
                    if(currentPageNumber < lastPageNumber) {
                        getData(pageNumber: currentPageNumber + 1)
                    }
                } else {
                    showNoNetworkConnectedMessage()
                }
            }
        }
    }
}






