//
//  BaseViewController.swift
//  PhotoBrowser
//
//  Created by Mo'men on 11/1/17.
//  Copyright © 2017 Mo'men. All rights reserved.
//

import UIKit
import Reachability
import SwiftMessages

class BaseViewController: UIViewController {

    
    // MARK: - Variables
    
    let imageSession = URLSession(configuration: .default)
    var refreshControl: UIRefreshControl?

    
    // MARK: - Cell Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewBackgroundImage()
        setupSwipeRefresh()
    }

    
    // MARK: - Screen Style
    
    func setViewBackgroundImage() {
        self.view?.backgroundColor = UIColor(patternImage: UIImage(named: ConstantsImages.IC_BACKGROUND)!)
    }
    
    
    // MARK: Refrish control setup
    
    func setupSwipeRefresh() -> Void {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshControl?.tintColor = UIColor.white
    }

    
    func refreshData() {
        if(isNetworkConnected()) {
            getData(pageNumber: 1)
        } else {
            self.refreshControl?.endRefreshing()
            showNoNetworkConnectedMessage()
        }
    }
    
    func getData(pageNumber: Int) {
        // This methode will override at sub classes
    }

    
    func checkRefrishControlState() -> Void {
        if (self.refreshControl?.isRefreshing)! {
            self.refreshControl?.endRefreshing()
        }
    }
    
    
    // MARK: - Network
    
    public func isNetworkConnected() -> Bool{
        let reachability = Reachability()!
        return reachability.isReachable
    }
    
    public func showNoNetworkConnectedMessage(){
        
        SwiftMessages.showMessage(title: Utilities.localizedString(forKey: "erroe"), body: Utilities.localizedString(forKey: "noNetworkConnection"), type: .error, layout: .cardView, buttonText: "", presentationStyle: .top, duration: 2)
    }
}

